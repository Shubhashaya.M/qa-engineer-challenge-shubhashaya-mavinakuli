import unittest
from mock import patch, Mock, call
from challenge.main import produce, consume


class TestProducerConsumer(unittest.TestCase):
    def test_produce_empty_lists_raises(self):
        with self.assertRaises(ValueError):
            produce(None, [1., 2.])
        with self.assertRaises(ValueError):
            produce([Mock(), Mock()], None)

    def test_produce_wrong_parameter_type_raises(self):
        mock_call = Mock(side_effect=ValueError)
        mock_call.__name__ = 'mock_area'
        with self.assertRaises(ValueError):
            produce([mock_call], ['a'])
        mock_call.assert_called_once_with('a')

    def test_produce_non_callable_figure_area_raises(self):
        with self.assertRaises(AttributeError):
            produce(['a', 'b'], [1., 2.])

    @patch('builtins.print')
    @patch('challenge.main.queue', return_value=Mock(spec=list))
    def test_produce_success(self, mock_q, mock_print):
        mock_q.append = Mock()
        mock_call = Mock(return_value=1.)
        mock_call.__name__ = 'mock_area'

        produce([mock_call, mock_call], [1., 2.])

        self.assertEqual(mock_print.call_count, 2)
        self.assertEqual(mock_call.call_count, 2)
        self.assertEqual(mock_q.append.call_count, 2)
